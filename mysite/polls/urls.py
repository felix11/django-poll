from django.conf.urls import include, url
from django.views.generic import TemplateView
from polls.view import question_view
from django.urls import path



app_name = 'polls'

urlpatterns = [
    path('', question_view.IndexView.as_view(), name='index'),
    path('<int:pk>/', question_view.DetailView.as_view(), name='detail'),
    path('<int:pk>/results/', question_view.ResultsView.as_view(), name='results'),
    path('<int:question_id>/vote/', question_view.vote, name='vote'),
]